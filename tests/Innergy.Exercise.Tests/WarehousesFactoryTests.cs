using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Innergy.Exercise.Tests
{
    [TestFixture]
    public class WarehousesFactoryTests
    {
        private WarehousesFactory _warehousesFactory;

        [SetUp]
        public void Setup()
        {
            _warehousesFactory = new WarehousesFactory();
        }

        [Test]
        public void Create_ShouldReturnValidWarehousesData_WhenThereAreSigleMaterialAssignedToSingleWarehouse()
        {
            // Arrange
            var inputLines = new List<InputLine>
            {
                new InputLine
                {
                    MaterialId = "#1",
                    MaterialName = "M #1",
                    WarehouseData = new[] { ("WH #1", 5) }.ToList()
                },
                new InputLine
                {
                    MaterialId = "#2",
                    MaterialName = "M #2",
                    WarehouseData = new[] { ("WH #2", 9) }.ToList()
                },
            };

            // Act
            var warehouses = _warehousesFactory.Create(inputLines);
            
            // Assert
            Assert.That(warehouses, Has.Count.EqualTo(2));
            Assert.That(warehouses[0].Name, Is.EqualTo("WH #2"));
            Assert.That(warehouses[0].MaterialsCount, Is.EqualTo(9));
            Assert.That(warehouses[0].Materials, Has.Count.EqualTo(1));
            Assert.That(warehouses[0].Materials[0].Name, Is.EqualTo("M #2"));
            Assert.That(warehouses[0].Materials[0].Id, Is.EqualTo("#2"));

            Assert.That(warehouses[1].Name, Is.EqualTo("WH #1"));
            Assert.That(warehouses[1].MaterialsCount, Is.EqualTo(5));
            Assert.That(warehouses[1].Materials, Has.Count.EqualTo(1));
            Assert.That(warehouses[1].Materials[0].Name, Is.EqualTo("M #1"));
            Assert.That(warehouses[1].Materials[0].Id, Is.EqualTo("#1"));
        }

        [Test]
        public void Create_ShouldReturnValidWarehousesData_WhenThereAreSigleMaterialAssignedToSingleWarehouse_AndMaterialCountEquals()
        {
            // Arrange
            var inputLines = new List<InputLine>
            {
                new InputLine
                {
                    MaterialId = "#1",
                    MaterialName = "M #1",
                    WarehouseData = new[] { ("AAA", 9) }.ToList()
                },
                new InputLine
                {
                    MaterialId = "#2",
                    MaterialName = "M #2",
                    WarehouseData = new[] { ("ZZZ", 9) }.ToList()
                },
            };

            // Act
            var warehouses = _warehousesFactory.Create(inputLines);
            
            // Assert
            Assert.That(warehouses, Has.Count.EqualTo(2));
            Assert.That(warehouses[0].Name, Is.EqualTo("ZZZ"));
            Assert.That(warehouses[0].MaterialsCount, Is.EqualTo(9));
            Assert.That(warehouses[0].Materials, Has.Count.EqualTo(1));
            Assert.That(warehouses[0].Materials[0].Name, Is.EqualTo("M #2"));
            Assert.That(warehouses[0].Materials[0].Id, Is.EqualTo("#2"));

            Assert.That(warehouses[1].Name, Is.EqualTo("AAA"));
            Assert.That(warehouses[1].MaterialsCount, Is.EqualTo(9));
            Assert.That(warehouses[1].Materials, Has.Count.EqualTo(1));
            Assert.That(warehouses[1].Materials[0].Name, Is.EqualTo("M #1"));
            Assert.That(warehouses[1].Materials[0].Id, Is.EqualTo("#1"));
        }

        
        [Test]
        public void Create_ShouldReturnValidWarehousesDataWithSortedMaterials_WhenThereAreMultipleMaterialAssignedToWarehouses()
        {
            // Arrange
            var inputLines = new List<InputLine>
            {
                new InputLine
                {
                    MaterialId = "bbb",
                    MaterialName = "M #1",
                    WarehouseData = new[] { ("AAA", 3) }.ToList()
                },
                new InputLine
                {
                    MaterialId = "ccc",
                    MaterialName = "M #2",
                    WarehouseData = new[] { ("ZZZ", 7) }.ToList()
                },
                new InputLine
                {
                    MaterialId = "ccc",
                    MaterialName = "M #2",
                    WarehouseData = new[] { ("AAA", 1) }.ToList()
                },
                new InputLine
                {
                    MaterialId = "ddd",
                    MaterialName = "M #3",
                    WarehouseData = new[] { ("ZZZ", 22) }.ToList()
                },
                new InputLine
                {
                    MaterialId = "eee",
                    MaterialName = "M #4",
                    WarehouseData = new[] { ("ZZZ", 5) }.ToList()
                },
            };

            // Act
            var warehouses = _warehousesFactory.Create(inputLines);
            
            // Assert
            Assert.That(warehouses, Has.Count.EqualTo(2));
            Assert.That(warehouses[0].Name, Is.EqualTo("ZZZ"));
            Assert.That(warehouses[0].MaterialsCount, Is.EqualTo(34));
            Assert.That(warehouses[0].Materials, Has.Count.EqualTo(3));
            
            Assert.That(warehouses[0].Materials[0].Name, Is.EqualTo("M #2"));
            Assert.That(warehouses[0].Materials[0].Id, Is.EqualTo("ccc"));
            
            Assert.That(warehouses[0].Materials[1].Name, Is.EqualTo("M #3"));
            Assert.That(warehouses[0].Materials[1].Id, Is.EqualTo("ddd"));

            Assert.That(warehouses[0].Materials[2].Name, Is.EqualTo("M #4"));
            Assert.That(warehouses[0].Materials[2].Id, Is.EqualTo("eee"));

            Assert.That(warehouses[1].Name, Is.EqualTo("AAA"));
            Assert.That(warehouses[1].MaterialsCount, Is.EqualTo(4));
            Assert.That(warehouses[1].Materials, Has.Count.EqualTo(2));
            
            Assert.That(warehouses[1].Materials[0].Name, Is.EqualTo("M #1"));
            Assert.That(warehouses[1].Materials[0].Id, Is.EqualTo("bbb"));

            Assert.That(warehouses[1].Materials[1].Name, Is.EqualTo("M #2"));
            Assert.That(warehouses[1].Materials[1].Id, Is.EqualTo("ccc"));
        }
    }
}
