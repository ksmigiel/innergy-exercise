using NUnit.Framework;

namespace Innergy.Exercise.Tests
{
    public class InputLineParserTests
    {
        private InputLineParser _parser;

        [SetUp]
        public void Setup()
        {
            _parser = new InputLineParser();
        }

        [Test]
        public void Parse_ShouldReturnNull_WhenInputStartsWithHashCharacter()
        {
            // Arrange
            var input = "#Test line with hash";

            // Act
            var parsedInput = _parser.Parse(input);

            // Assert
            Assert.That(parsedInput, Is.Null);
        }

        [Test]
        public void Parse_ShouldReturnNull_WhenInputStartsWithHashCharacterFollowedBySpace()
        {
            // Arrange
            var input = " #Test line with hash";

            // Act
            var parsedInput = _parser.Parse(input);

            // Assert
            Assert.That(parsedInput, Is.Null);
        }

        [Test]
        public void Parse_ShouldReturnParsedInputLine_WhenInputIsValidString()
        {
            // Arrange
            var input = "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10";

            // Act
            var parsedInput = _parser.Parse(input);

            // Assert
            Assert.That(parsedInput.MaterialName, Is.EqualTo("Cherry Hardwood Arched Door - PS"));
            Assert.That(parsedInput.MaterialId, Is.EqualTo("COM-100001"));
            Assert.That(parsedInput.WarehouseData, Has.Count.EqualTo(2));
            var (firstWarehouse, firstWarehouseItemsCount) = parsedInput.WarehouseData[0];
            Assert.That(firstWarehouse, Is.EqualTo("WH-A"));
            Assert.That(firstWarehouseItemsCount, Is.EqualTo(5));
            var (secondWarehouse, secondWarehouseItemsCount) = parsedInput.WarehouseData[1];
            Assert.That(secondWarehouse, Is.EqualTo("WH-B"));
            Assert.That(secondWarehouseItemsCount, Is.EqualTo(10));
        }

        [Test]
        public void Parse_ShouldReturnParsedInputLine_WhenOnlyOneWarehouseIsProvided()
        {
            // Arrange
            var input = "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5";

            // Act
            var parsedInput = _parser.Parse(input);

            // Assert
            Assert.That(parsedInput.MaterialName, Is.EqualTo("Cherry Hardwood Arched Door - PS"));
            Assert.That(parsedInput.MaterialId, Is.EqualTo("COM-100001"));
            Assert.That(parsedInput.WarehouseData, Has.Count.EqualTo(1));
            var (firstWarehouse, firstWarehouseItemsCount) = parsedInput.WarehouseData[0];
            Assert.That(firstWarehouse, Is.EqualTo("WH-A"));
            Assert.That(firstWarehouseItemsCount, Is.EqualTo(5));
        }
    }
}
