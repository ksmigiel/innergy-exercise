using System;
using System.Collections.Generic;
using System.Linq;

namespace Innergy.Exercise
{
    public class WarehousesFactory
    {
        public List<Warehouse> Create(IEnumerable<InputLine> inputLines)
        {
            var warehouses = new Dictionary<string, Dictionary<string, Material>>();

            foreach (var materialLine in inputLines)
            {
                foreach (var (warehouse, count) in materialLine.WarehouseData)
                {
                    if (warehouses.ContainsKey(warehouse))
                    {
                        var materials = warehouses[warehouse];

                        if (materials.ContainsKey(materialLine.MaterialId))
                        {
                            materials[materialLine.MaterialId].Quantity += count;
                        }
                        else
                        {
                            materials.Add(materialLine.MaterialId, new Material { Name = materialLine.MaterialName, Id = materialLine.MaterialId, Quantity = count });
                        }
                    }
                    else
                    {
                        warehouses.Add(warehouse, new Dictionary<string, Material>
                        {
                            [warehouse] = new Material { Name = materialLine.MaterialName, Id = materialLine.MaterialId, Quantity = count }
                        });
                    }
                }
            }

            return warehouses
                .Select(w => new Warehouse { Name = w.Key, Materials = w.Value.Values.OrderBy(m => m.Id).ToList() })
                .OrderByDescending(w => w.MaterialsCount)
                .ThenByDescending(w => w.Name)
                .ToList();
        }
    }
}
