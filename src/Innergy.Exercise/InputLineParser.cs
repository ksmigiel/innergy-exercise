using System;
using System.Linq;

namespace Innergy.Exercise
{
    public class InputLineParser
    {
        public InputLine Parse(string input)
        {
            input = input.Trim();
            
            if (input.StartsWith('#') || string.IsNullOrEmpty(input))
            {
                return null;
            }
            
            var inputParts = input.Split(';');

            var materialName = inputParts[0];
            var materialId = inputParts[1];

            var warehousesPart = inputParts[2].Split('|');

            return new InputLine
            {
                MaterialName = materialName,
                MaterialId = materialId,
                WarehouseData = warehousesPart.Select(wp => 
                {
                    var warehouseParts = wp.Split(',');
                    return (warehouseParts[0], int.Parse(warehouseParts[1]));
                }).ToList()
            };
        }
    }
}
