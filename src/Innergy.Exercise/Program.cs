﻿using System;
using System.Collections.Generic;

namespace Innergy.Exercise
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputLineParser = new InputLineParser();

            var inputLines = new List<InputLine>();

            string line;
            while ((line = Console.In.ReadLine()) != null )
            {
                var parsedLine = inputLineParser.Parse(line);
                if (parsedLine != null) inputLines.Add(parsedLine);
            }
            
            var warehouses = new WarehousesFactory().Create(inputLines);

            var isFirstLine = true;
            
            foreach (var warehouse in warehouses)
            {
                if (!isFirstLine) Console.Out.WriteLine();
                if (isFirstLine) isFirstLine = false;

                Console.Out.WriteLine($"{ warehouse.Name } (total { warehouse.MaterialsCount })");

                foreach (var material in warehouse.Materials)
                {
                    Console.Out.WriteLine($"{ material.Id }: { material.Quantity }");
                }
            }
        }
    }
}
