using System;
using System.Collections.Generic;
using System.Linq;

namespace Innergy.Exercise
{
    public class Warehouse
    {
        public string Name { get; set; }
        public int MaterialsCount => Materials.Sum(m => m.Quantity);
        public List<Material> Materials { get; set; }
    }
}
