using System;

namespace Innergy.Exercise
{
    public class Material
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}
