using System;
using System.Collections.Generic;

namespace Innergy.Exercise
{
    public class InputLine
    {
        public string MaterialName { get; set; }
        public string MaterialId { get; set; }
        public List<(string, int)> WarehouseData { get; set; }
    }
}
